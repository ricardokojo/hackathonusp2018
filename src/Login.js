import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import Logo from './images/ACR.png';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;

    this.setState({
      [name]: val,
    });
  }

  render() {
    return (
      <section>
        <div className='toparea'>
          <img className='logo' src={Logo} alt='logo' />
          <NavLink to="/login"><button className='login-btn'>Login</button></NavLink>
        </div>
        <div className="login-form">
          <form onSubmit={(e) => this.props.handleLogin(e, this.state)}>
            <img className='logo' src={Logo} alt='logo' />
            <div className='form-input'>
              <input 
                type="text"
                name="username"
                placeholder="Username"
                value={this.state.username}
                onChange={this.handleChange}
              />
            </div>
            <div className='form-input'>
              <input 
                type="password"
                name="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </div>
            <button>Log in!</button>
            <p>Get password</p>
          </form>
        </div>
      </section>
    )
  }
}

export default Login;