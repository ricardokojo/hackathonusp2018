import React, { Component } from 'react';
import axios from 'axios';

import Placeholder from './images/user-placeholder.jpg'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            user: null
        };

        var url = 'http://localhost:3004/pessoas?id=' + this.state.id;

        axios.get(url).then(response => {
            this.setState({
                user: response.data
            });
        });
    }

  render(){
    return(
      <section>
        <div className="profile">
          <div className="profile-placeholder">
            <img src={this.state.user == null ? Placeholder : (this.state.user[0].img != "" ? this.state.user[0].img : Placeholder)} alt='profile pic' className="profile-pic" />
          </div>
          <div className="profile-content">
            <h2 className="profile-name">
              {this.state.user == null ? '-' : this.state.user[0].name}
            </h2>
            <hr />
            <p className="profile-location">
              {this.state.user == null ? '-' : this.state.user[0].place}
            </p>
            <p className="profile-area">
              <b>Área de Atuação/Interesse:</b> {this.state.user == null ? '-' : this.state.user[0].area}
            </p>
            <p className="profile-phone">
              <b>Telefone:</b> {this.state.user == null ? '-' : this.state.user[0].phone}
            </p>
            <p className="profile-email">
              <b>Email:</b> {this.state.user == null ? '-' : this.state.user[0].email}
            </p>
            <p>
              <b>Publicações:</b> {this.state.user == null ? '-' : this.state.user[0].publi}
            </p>
            <p>
              <b>Projetos:</b> {this.state.user == null ? '-' : this.state.user[0].proj}
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default Profile;