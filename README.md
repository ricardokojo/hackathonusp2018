# ACR - Academic Collaborative Research

## Setup server

This project uses [json-server](https://github.com/typicode/json-server)

To install, run:

```
$ npm install -g json-server
```

To setup and run the server:

```
~hackathonusp2018$ cd db
~hackathonusp2018/db$ json-server --watch db.json --port 3004
```

## Setup project

This project uses [Docker](https://www.docker.com/)

Install Docker:

* Mac: https://docs.docker.com/docker-for-mac/install/
* Windows: https://docs.docker.com/docker-for-windows/install/
* Ubuntu: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
* Debian: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
* Arch Linux: https://wiki.archlinux.org/index.php/Docker

Install Docker-Compose:

* Docker-Compose: https://docs.docker.com/compose/install/

To setup and run the app:

```
docker-compose build
docker-compose up
```