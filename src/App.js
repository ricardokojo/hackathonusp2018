import React, { Component } from 'react';
import axios from 'axios';

import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import Home from "./Home";
import Login from "./Login";
import Profile from "./Profile"
import NotFoundPage from './NotFoundPage';
import Dashboard from './dashboard';

class App extends Component {
  constructor() {
    super();
    this.state = {
      logged: false,
      dashboardUrl: ''
    };
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(e, data) {
    var url = 'http://localhost:3004/users?username=';
    url += data.username;

    e.preventDefault();
    axios.get(url).then(response => {
      this.setState({
        user: response.data
      });

      var url = '/dashboard/' + this.state.user[0].id;
      if (this.state.user.length === 1) {
        this.setState({
          logged: true,
          dashboardUrl: url
        });
      } else {
        console.log('Usuário inválido');
      }
    }).catch(err => {
      console.log(err);
    });
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route path="/" component={Home} exact={true} />
            <Route 
              path="/login" 
              render={
                () => (this.state.logged)
                ? <Redirect to={this.state.dashboardUrl}/>
                : <Login handleLogin={this.handleLogin}/>
              }
            />
            <Route path="/profile/:id" component={Profile} />
            <Route path="/dashboard/:id" component={Dashboard} exact={true} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;