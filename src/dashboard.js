import React, { Component } from 'react';
import axios from 'axios';
import {NavLink} from 'react-router-dom';   

import Logo from './images/ACR.png';
import Loupe from './images/loupe.svg';
import Card from './card';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            user: {
                name: '',
                place: '',
                area: '',
                phone: '',
                email: '',
                publi: '',
                proj: '',
            }
        };

        var url = 'http://localhost:3004/pessoas?user=' + this.state.id;
        axios.get(url).then(response => {
            this.setState({
                user: response.data[0]
            });
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        var urlPost = 'http://localhost:3004/pessoas/' + this.state.user.id;
        
        this.state.user.rank += 5;

        axios.put(urlPost, this.state.user).then(response => {
            console.log(response);
            alert("Perfil atualizado com sucesso!")
        }).catch(err => {
            console.log(err);
        })
    }

    handleChange(e) {
        var property = {...this.state.user}
        const name = e.target.name;
        const val = e.target.value;
        property[name] = val;

        this.setState({
            user: property,
        });
    }

    render() {
        return(
            <section id='dashboard'>
                <div className='toparea-shadow'>
                    <a href='/'> <img className='logo' src={Logo} alt='logo' /> </a>
                    <div className='searchbar'>
                        <input name='searchArea' type='text' placeholder="Área de Pesquisa" />
                        <button className='search-btn' onClick={this.search}><img src={Loupe} className='icon' /></button>
                    </div>
                    <div className="profile-data">
                        <p> Olá {this.state.user.name}, bem vindo(a). </p>
                        <img src="http://placekitten.com/300/300" />
                    </div>
                </div>

                <div className="dashboard">
                    <div className="dash-profile">
                        <h3>Meu Perfil</h3>
                        <hr />
                        <form className='dash-form' onSubmit={this.handleSubmit}>
                            <div className='form-group'>
                                <h4>Local</h4>
                                <input 
                                    type="text"
                                    name="user.place"
                                    placeholder="Local"
                                    value={this.state.user.place}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className='form-group'>
                                <h4>Área de Atuação/Interesses:</h4>
                                <input 
                                    type="text"
                                    name="area"
                                    placeholder="Área de atuação ou interesses"
                                    value={this.state.user.area}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className='form-group'>
                                <h4>Telefone:</h4>
                                <input 
                                    type="text"
                                    name="phone"
                                    placeholder="(11) 91234-5678"
                                    value={this.state.user.phone}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className='form-group'>
                                <h4>E-mail:</h4> 
                                <input 
                                    type="text"
                                    name="email"
                                    placeholder="exemplo@usp.br"
                                    value={this.state.user.email}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className='form-group'>
                                <h4>Publicações:</h4> 
                                <textarea 
                                    type="text"
                                    name="publi"
                                    value={this.state.user.publi}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <div className='form-group'>
                                <h4>Projetos:</h4> 
                                <textarea 
                                    type="text"
                                    name="proj"
                                    value={this.state.user.proj}
                                    onChange={this.handleChange}
                                />
                            </div>

                            <button>Atualizar</button>
                        </form>
                    </div>
                    <div className="dash-matches">
                        <h3>Pode te interessar...</h3>
                        <hr />
                        <Card data={ {"name": "André Fujita - Docente", "area":"Sistemas biológicos, neurociência" } } /> 
                        <Card data={ {"name": "João Eduardo Ferreira - Docente", "area":"Armazenamento de dados, bioinformática"  } } /> 
                        <Card data={ {"name": "Marcel Jackowski - Docente", "area":"Processamento de sinais, bioinformática" } } /> 
                        <Card data={ {"name": "Otavio Aragoni - Discente", "area":"Bioinformatica, machine learning" } } /> 
                    </div>
                </div>
            </section>
        );
    }
}

export default Dashboard;