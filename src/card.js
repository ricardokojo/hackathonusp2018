import React, { Component } from 'react';
import Placeholder from './images/user-placeholder.jpg'

class Card extends Component {
    constructor() {
        super();
    }

    render() {
        var url = 'http://localhost:3000/profile/' + this.props.data.id;
        return(
            <a href={url}>
                <div className="card">
                    <img className="card-pic" src={this.props.data.img != "" ? this.props.data.img : Placeholder}/> 
                    <div className="card-content">
                        <h3 className="card-title">{ this.props.data.name }</h3>
                        <hr />
                        <p className="card-description">Área de Conhecimento: { this.props.data.area }</p>
                    </div>
                </div>
            </a>
        );
    }
}

export default Card;