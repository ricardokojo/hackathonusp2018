import React, { Component } from 'react';
import Card from './card';

class SearchResult extends Component {
    constructor() {
        super();
    }

    render() {
        return(
            <div className="search-result-area">
                <h2>{this.props.searchArea}</h2>
                {(this.props.result != null) 
                ? this.props.result.map(r => {
                    return (
                        <Card data={r}/>
                    );
                })
                : <p>Loading...</p>
                }
            </div>
        );
    }
}

export default SearchResult;