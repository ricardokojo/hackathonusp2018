import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import axios from 'axios';

import Logo from './images/ACR.png';
import Loupe from './images/loupe.svg';
import ChevronDown from './images/chevron-arrow-down.svg';
import SearchResult from './search-result';

var filter = function(jsonobj, search){
  var toret = [];
  for (var i = 0; i < jsonobj.length; i++) {
      var areas = jsonobj[i].area.split(", ");
      for (var j = 0; j < areas.length; j++) {
          if (areas[j] === search) {
              toret.push(jsonobj[i]);
          }
      }
  }
  console.log(toret);
  return toret;
}

class Home extends Component {
  constructor() {
    super();
    this.state = {
      searchArea: '',
      userType: 0,
      searchInterest: 0,
    };
    this.handleChange = this.handleChange.bind(this);
    this.showFilters = this.showFilters.bind(this);
    this.search = this.search.bind(this);
  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;

    this.setState({
      [name]: val,
    });
  }

  showFilters(e) {
    if (this.myInput.className === 'filters')
      this.myInput.className = 'filters-active';
    else
      this.myInput.className = 'filters';
  }

  search(e) {
    this.searchDiv.className = 'searcharea shrink';
    var url = 'http://localhost:3004/pessoas';
    var checkType = false;

    if(this.state.userType == 1) {
      // discentes
      url += '?tipo=1';
      checkType = true;
    } else if (this.state.userType == 2) {
      // docentes
      url += '?tipo=0';
      checkType = true;
    }

    if (checkType == true)
      url += '&_sort=rank&_order=desc';
    else
      url += '?_sort=rank&_order=desc';

    console.log(url);

    e.preventDefault();
    axios.get(url).then(response => {
      
      let aux = response.data;
      if (this.state.searchArea != ""){
        aux = filter(response.data, this.state.searchArea);
      }

      this.setState({
        result: aux
      });
      console.log(this.state);
    }).catch(err => {
      console.log(err);
    })
  }
  
  render() {
    return (
      <section id='search' className="search">
        <div className='toparea'>
          <img className='logo' src={Logo} alt='logo' />
          <NavLink to="/login"><button className='login-btn'>Login</button></NavLink>
        </div>
        <div className='searcharea' ref={div => {
            this.searchDiv = div;}}>
          <div className='searchbar'>
            <p className='text'>
              Tenho interesse em <input name='searchArea' type='text' placeholder="Área de Pesquisa"
                value={this.state.searchArea}
                onChange={this.handleChange} />
            </p>
            <button className='search-btn' onClick={this.search}><img src={Loupe} className='icon' /></button>
          </div>

          <button id='filterButton' className='filter-btn' onClick={this.showFilters}>
            Filtros <img className='icon-small' src={ChevronDown} />
          </button>
          <div id='filters' className='filters' ref={input => {
            this.myInput = input;
          }}>
            <p>
              Procuro por 
              <select id='searchInterest' name='searchInterest' value={this.state.searchInterest}
              onChange={this.handleChange}>
                <option value='0'>tudo</option>
                <option value='1'>especialidade</option>
                <option value='2'>projeto atual</option>
                <option value='3'>projetos recentes</option>
              </select>
              de
              <select id='userType' name='userType' value={this.state.userType}
              onChange={this.handleChange}>
                <option value='0'>qualquer</option>
                <option value='1'>discentes</option>
                <option value='2'>docentes</option>
              </select>
            </p>
          </div>
        </div>

        <SearchResult searchArea={this.state.searchArea} result={this.state.result}/>
      </section>
    );
  }
}

export default Home;
